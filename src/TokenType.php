<?php declare(strict_types=1);

namespace Eternaltwin\OauthClient;

final class TokenType implements \JsonSerializable {
  private static ?self $_Bearer;

  private string $inner;

  final private function __construct(string $inner) {
    $this->inner = $inner;
  }

  final public function jsonSerialize(): string {
    return $this->inner;
  }

  final public function toString(): string {
    return $this->inner;
  }

  final public static function jsonDeserialize($raw): self {
    return self::fromString($raw);
  }

  final public static function fromString(string $raw): self {
    switch ($raw) {
      case "Bearer":
        return self::Bearer();
      default:
        throw new \TypeError("Unexpected `TokenType` value");
    }
  }

  final public static function Bearer(): self {
    if (!isset(self::$_Bearer)) {
      self::$_Bearer = new self("Bearer");
    }
    return self::$_Bearer;
  }
}

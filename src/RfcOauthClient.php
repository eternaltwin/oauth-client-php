<?php declare(strict_types=1);

namespace Eternaltwin\OauthClient;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\UriInterface;

final class RfcOauthClient {
  const DEFAULT_TIMEOUT = 30.0;

  private Uri $authorizationEndpoint;
  private Uri $tokenEndpoint;
  private Uri $callbackEndpoint;
  private string $clientId;
  private string $clientSecret;

  private Client $client;

  final public function __construct(
    string $authorizationEndpoint,
    string $tokenEndpoint,
    string $callbackEndpoint,
    string $clientId,
    string $clientSecret
  ) {
    $this->authorizationEndpoint = new Uri($authorizationEndpoint);
    $this->tokenEndpoint = new Uri($tokenEndpoint);
    $this->callbackEndpoint = new Uri($callbackEndpoint);
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->client = new Client(["timeout" => self::DEFAULT_TIMEOUT]);
  }

  /**
   * @param string $scope Scope string.
   * @param string $state Client state.
   * @return UriInterface Authorization URI where the user should be redirected.
   */
  final public function getAuthorizationUri(string $scope, string $state): UriInterface {
    return Uri::withQueryValues(
      $this->authorizationEndpoint,
      [
        "access_type" => "offline",
        "response_type" => "code",
        "redirect_uri" => $this->callbackEndpoint,
        "client_id" => $this->clientId,
        "scope" => $scope,
        "state" => $state,
      ],
    );
  }

  /**
   * @param string $code One-time authorization code.
   * @return AccessToken OAuth access token.
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \JsonException
   */
  final public function getAccessTokenSync(string $code): AccessToken {
    $res = $this->client->post(
      $this->tokenEndpoint,
      [
        RequestOptions::HEADERS => [
          "Authorization" => $this->getAuthorizationHeader(),
        ],
        RequestOptions::JSON => [
          "client_id" => $this->clientId,
          "client_secret" => $this->clientSecret,
          "code" => $code,
          "grant_type" => "authorization_code",
        ]
      ],
    );
    $resBody = $res->getBody()->getContents();
    return AccessToken::fromJson($resBody);
  }

  private function getAuthorizationHeader(): string {
    return "Basic " . base64_encode($this->clientId . ":" . $this->clientSecret);
  }
}
